"      ___                              ___         ___         ___
"     /  /\        ___      ___        /__/\       /  /\       /  /\
"    /  /:/_      /__/\    /  /\      |  |::\     /  /::\     /  /:/
"   /  /:/ /\     \  \:\  /  /:/      |  |:|:\   /  /:/\:\   /  /:/
"  /  /:/_/::\     \  \:\/__/::\    __|__|:|\:\ /  /:/~/:/  /  /:/  ___
" /__/:/__\/\:\___  \__\:\__\/\:\__/__/::::| \:/__/:/ /:/__/__/:/  /  /\
" \  \:\ /~~/:/__/\ |  |:|  \  \:\/\  \:\~~\__\\  \:\/:::::\  \:\ /  /:/
"  \  \:\  /:/\  \:\|  |:|   \__\::/\  \:\      \  \::/~~~~ \  \:\  /:/
"   \  \:\/:/  \  \:\__|:|   /__/:/  \  \:\      \  \:\      \  \:\/:/
"    \  \::/    \__\::::/    \__\/    \  \:\      \  \:\      \  \::/
"     \__\/         ~~~~               \__\/       \__\/       \__\/
"
" http://patorjk.com/software/taag/ - Isometric 3 Smush (U)
"
" Maintained: sgur <sgurrr+vim@gmail.com>
"

scriptencoding utf-8

" syntax, highlighting and spelling {{{1
augroup gvimrc_loading
  autocmd!
  " sierra SignColumn improvement
  autocmd ColorScheme sierra  if synIDattr(synIDtrans(hlID("SignColumn")), "bg") ==# 'Grey'
        \ | highlight! link SignColumn FoldColumn
        \ | endif
  if has('multi_byte_ime')
    autocmd ColorScheme *
          \   call s:link_hlgroup_to_cursor('lCursor')
          \ | call s:link_hlgroup_to_cursor('CursorIM')
  endif
augroup END

" IMEやXIMがオンのときのカーソルの色 (GUI) {{{2
function! s:link_hlgroup_to_cursor(hlgroup) abort "{{{
  let hl_id = synIDtrans(hlID(a:hlgroup))
  if !hlexists(a:hlgroup) || synIDattr(hl_id, 'fg') ==# 'bg' && synIDattr(hl_id, 'bg') ==# 'fg'
    execute 'highlight! link' a:hlgroup 'Cursor'
  endif
endfunction "}}}

" using the mouse {{{1
set mousehide

augroup gvimrc_menu
  autocmd!
augroup END

" ツールバー読み込み "{{{2
if exists('##OptionSet')
  autocmd gvimrc_menu OptionSet guioptions
        \   if v:option_new =~# 'T'
        \ |   runtime menu/toolbar.vim
        \ | endif
endif

" ポップアップメニュー {{{2
let s:cmd_menutrans = printf('runtime lang/menu_%s.%s.vim', tolower(!empty(&langmenu) ? &langmenu : v:lang), tolower(&encoding))
autocmd gvimrc_menu MenuPopup *
      \   execute s:cmd_menutrans
      \ | runtime menu/popup.vim

if exists('##OptionSet')
  autocmd gvimrc_menu OptionSet guioptions
        \   if v:option_new =~# 'm'
        \ |   execute s:cmd_menutrans
        \ |   runtime menu.vim
        \ | endif
endif

" 透明化 {{{2
if exists('+transparency')
  command! TransparencyEnable  call vimrc#transparency#set_state(1)
  command! TransparencyDisable  call vimrc#transparency#set_state(0)
endif

" DirectDraw {{{2
if has('directx') && empty(&renderoptions)
  augroup gvimrc_renderoptions
    autocmd!
    autocmd ColorScheme *
          \ let &renderoptions = printf('type:directx,gamma:%1.1f,contrast:3,level:0.8,geom:1,renmode:5', &background is# 'light' ? 1.6 : 1.9)
  augroup END
endif

" GUI Options {{{2
set guioptions-=a guioptions+=A guioptions+=c guioptions-=e guioptions-=m
set guioptions-=t guioptions-=T guioptions-=r guioptions-=L
" 1}}}
