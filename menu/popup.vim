scriptencoding utf-8

if !(has("menu") && has('gui_running'))
      \ || (exists('g:loaded_vimrc_popup') && g:loaded_vimrc_popup)
  finish
endif
let g:loaded_vimrc_popup = 1

try
  aunmenu PopUp
catch /^Vim\%((\a\+)\)\=:E329/
finally
endtry

anoremenu 1.10 PopUp.&Undo u
anoremenu 1.15 PopUp.-SEP1- <Nop>
vnoremenu 1.20 PopUp.Cu&t "+x
vnoremenu 1.30 PopUp.&Copy "+y
cnoremenu 1.30 PopUp.&Copy <C-Y>
nnoremenu 1.40 PopUp.&Paste "+gP
cnoremenu 1.40 PopUp.&Paste <C-R>+
execute 'vnoremenu <script> 1.40 PopUp.&Paste ' . g:paste#paste_cmd['v']
execute 'inoremenu <script> 1.40 PopUp.&Paste ' . g:paste#paste_cmd['i']
vnoremenu 1.50 PopUp.&Delete x
anoremenu 1.55 PopUp.-SEP2- <Nop>
vnoremenu 1.60 PopUp.Select\ Blockwise <C-V>

nnoremenu 1.70 PopUp.Select\ &Word vaw
onoremenu 1.70 PopUp.Select\ &Word aw
vnoremenu 1.70 PopUp.Select\ &Word <C-C>vaw
inoremenu 1.70 PopUp.Select\ &Word <C-O>vaw
cnoremenu 1.70 PopUp.Select\ &Word <C-C>vaw

nnoremenu 1.73 PopUp.Select\ &Sentence vas
onoremenu 1.73 PopUp.Select\ &Sentence as
vnoremenu 1.73 PopUp.Select\ &Sentence <C-C>vas
inoremenu 1.73 PopUp.Select\ &Sentence <C-O>vas
cnoremenu 1.73 PopUp.Select\ &Sentence <C-C>vas

nnoremenu 1.77 PopUp.Select\ Pa&ragraph vap
onoremenu 1.77 PopUp.Select\ Pa&ragraph ap
vnoremenu 1.77 PopUp.Select\ Pa&ragraph <C-C>vap
inoremenu 1.77 PopUp.Select\ Pa&ragraph <C-O>vap
cnoremenu 1.77 PopUp.Select\ Pa&ragraph <C-C>vap

nnoremenu 1.80 PopUp.Select\ &Line V
onoremenu 1.80 PopUp.Select\ &Line <C-C>V
vnoremenu 1.80 PopUp.Select\ &Line <C-C>V
inoremenu 1.80 PopUp.Select\ &Line <C-O>V
cnoremenu 1.80 PopUp.Select\ &Line <C-C>V

nnoremenu 1.90 PopUp.Select\ &Block <C-V>
onoremenu 1.90 PopUp.Select\ &Block <C-C><C-V>
vnoremenu 1.90 PopUp.Select\ &Block <C-C><C-V>
inoremenu 1.90 PopUp.Select\ &Block <C-O><C-V>
cnoremenu 1.90 PopUp.Select\ &Block <C-C><C-V>

noremenu  <script> <silent> 1.100 PopUp.Select\ &All :<C-U>call <SID>select_all()<CR>
inoremenu <script> <silent> 1.100 PopUp.Select\ &All <C-O>:call <SID>select_all()<CR>
cnoremenu <script> <silent> 1.100 PopUp.Select\ &All <C-U>call <SID>select_all()<CR>

function! s:select_all()
  execute "normal! gg" . (&selectmode == "" ? "VG" : "gH\<C-O>G")
endfunction
