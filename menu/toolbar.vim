scriptencoding utf-8

if !(has("menu") && has("toolbar") && has('gui_running'))
      \ || (exists('g:loaded_vimrc_toolbar') && g:loaded_vimrc_toolbar)
  finish
endif
let g:loaded_vimrc_toolbar = 1

try
  aunmenu ToolBar
catch /^Vim\%((\a\+)\)\=:E329/
endtry

anoremenu 1.10 ToolBar.Open		:browse confirm e<CR>
anoremenu <silent> 1.20 ToolBar.Save	:if expand("%") == ""<Bar>browse confirm w<Bar>else<Bar>confirm w<Bar>endif<CR>

if has("printer")
  anoremenu 1.40   ToolBar.Print	:hardcopy<CR>
  vunmenu   ToolBar.Print
  vnoremenu ToolBar.Print		:hardcopy<CR>
elseif has("unix")
  anoremenu 1.40   ToolBar.Print	:w !lpr<CR>
  vunmenu   ToolBar.Print
  vnoremenu ToolBar.Print		:w !lpr<CR>
endif

anoremenu 1.45 ToolBar.-sep1-		<Nop>
anoremenu 1.50 ToolBar.Undo		u
anoremenu 1.60 ToolBar.Redo		<C-R>

anoremenu 1.65 ToolBar.-sep2-		<Nop>
vnoremenu 1.70 ToolBar.Cut		"+x
vnoremenu 1.80 ToolBar.Copy		"+y
cnoremenu 1.80 ToolBar.Copy		<C-Y>
nnoremenu 1.90 ToolBar.Paste		"+gP
cnoremenu      ToolBar.Paste		<C-R>+
execute 'vnoremenu <script> ToolBar.Paste' g:paste#paste_cmd['v']
execute 'inoremenu <script> ToolBar.Paste' g:paste#paste_cmd['i']

anoremenu 1.295 ToolBar.-sep3-		<Nop>
anoremenu 1.300 ToolBar.Help			:help<CR>
anoremenu <silent> 1.310 ToolBar.FindHelp	:call <SID>Helpfind()<CR>

function! s:Helpfind()
  let h = input('Enter a command o word to find help on: ', expand('<cword>'), 'help')
  if h isnot# ''
    silent! execute "help " . h
  endif
endfunction
