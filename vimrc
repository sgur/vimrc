"                           ___         ___         ___
"      ___      ___        /__/\       /  /\       /  /\
"     /__/\    /  /\      |  |::\     /  /::\     /  /:/
"     \  \:\  /  /:/      |  |:|:\   /  /:/\:\   /  /:/
"      \  \:\/__/::\    __|__|:|\:\ /  /:/~/:/  /  /:/  ___
"  ___  \__\:\__\/\:\__/__/::::| \:/__/:/ /:/__/__/:/  /  /\
" /__/\ |  |:|  \  \:\/\  \:\~~\__\\  \:\/:::::\  \:\ /  /:/
" \  \:\|  |:|   \__\::/\  \:\      \  \::/~~~~ \  \:\  /:/
"  \  \:\__|:|   /__/:/  \  \:\      \  \:\      \  \:\/:/
"   \__\::::/    \__\/    \  \:\      \  \:\      \  \::/
"       ~~~~               \__\/       \__\/       \__\/
"
" http://goo.gl/n7SDvY - Isometric 3 Smush (U)
" Maintained: sgur <sgurrr+vim@gmail.com>

" common {{{1
source <sfile>:h/macros/vimrc.encode_japan
scriptencoding utf-8

let s:rc_dir = expand('<sfile>:p:h:gs?\?/?')

if has('vim_starting')
  if has('timers') && has('lambda')
    command! -nargs=1 Defer  call timer_start(1000, {_ -> <args>})
  else
    augroup vimrc_defer_initialization
      autocmd!
    augroup END
    command! -nargs=1 Defer
          \ autocmd vimrc_defer_initialization VimEnter *  call <args>
  endif
else
  command! -nargs=1 Defer  echo ''
endif

" options {{{1
set ambiwidth=double
set backspace+=indent backspace+=eol backspace+=start " defaults.vim
set backupdir^=$HOME/.cache/vim/backup
if has('linebreak') && exists('&breakindentopt')
  set breakindent breakindentopt=min:20,shift:4,sbr
endif
set completeopt=menuone,longest,preview
if has('cscope') && vimrc#executable('gtags-cscope')
  set cscopetag cscopeprg=gtags-cscope
  set cscopequickfix=s-,c-,d-,i-,t-,e-
endif
if has('patch-7.4.2109')
  set display+=truncate " defaults.vim
endif
let &dictionary=expand(s:rc_dir . '/etc/look/words')
set directory^=$HOME/.cache/vim/swap//
set noequalalways
set foldcolumn=1
set formatoptions+=B formatoptions+=m formatoptions+=j
let &grepprg = vimrc#executable('jvgrep') ? 'jvgrep --color never -n -I' . (&encoding is# 'utf-8' ? ' -8' : '')
      \ : vimrc#executable('git') ? 'git grep --no-index --exclude-standard -I --line-number --no-color'
      \   : vimrc#executable('grep') ? 'grep -InH' : 'internal'
set guioptions+=M
set history=200 " defaults.vim
set hlsearch
set iminsert=0 imsearch=0
set incsearch " defaults.vim
set ignorecase infercase smartcase
if exists('&langnoremap')
  set langnoremap nolangremap " defaults.vim
endif
set laststatus=2
set matchpairs+=（:）,｛:｝,「:」,［:］,『:』,【:】
let &shelltemp = !has('win32') || !has('filterpipe')
set shiftwidth=0
let &showbreak = g:vimrc#has_utf8_symbol ? '￫ ' : '> '
set nrformats-=octal " defaults.vim
if exists('&signcolumn')
  set signcolumn=yes
endif
set smartindent autoindent
set smarttab
set spelllang& spelllang+=cjk " Prevent duplication
let &spellfile=expand(s:rc_dir . '/spell/' . &encoding . '.add')
set tabline=%!tabline#format()
set tags+=tags;,.git/tags
set ttimeout ttimeoutlen=100
set undodir^=$HOME/.cache/vim/undo
set undofile
set visualbell
set virtualedit+=block
set viewdir=$HOME/.cache/vim/view
set viminfo=!,'600,<300,s60,h,rA:,rB:
set wildignore+=*.o,*.obj,*.so,*.out,*.jar,*.py?,*.luac,*.sw?,*.tmp,*.db,cscope*.out
if has('win32')
  set wildignore+=desktop.ini,NTUSER*,ntuser*,*.sys,*.pdb,*.pch,*.dcu
elseif has('mac')
  set wildignore+=.DS_Store
endif
set wildignorecase
set wildmenu wildmode=longest:full

" reset {{{2
if !has('vim_starting') && !empty(&l:filetype)
  edit %
endif

" terminal {{{1
if has('vim_starting') && !has('gui_running')
  if !has('win32') && has('vertsplit') && $TERM !=# 'vt100'
    source <sfile>:h/macros/vimrc.vertsplit
  endif
  if exists('$SSH_CONNECTION')
    source <sfile>:h/macros/vimrc.toggle_ime_on_ssh
  endif
  if &term =~# 'xterm' && !has('patch-8.0.0210') " xterm-bracketed-paste
    source <sfile>:h/macros/vimrc.bracket_paste
  endif
endif

" system specific {{{1
if has('win32')
  " MSYSから起動した場合に shell を cmd.exe に戻す {{{2
  if exists('$COMSPEC') && exists('$MSYSTEM')
    let $SHELL=$COMSPEC
    let &shell=$COMSPEC
  endif " 2}}}
else
  " fish shell 利用時も Vim では (ba)sh を利用 {{{2
  if $SHELL =~# '\<fish$'
    let $SHELL=system('which bash')[:-2]
    let &shell=$SHELL
  endif " 2}}}
  if has('unix')
    " Shared Memory が有効ならそこに tempfile を出力 (http://goo.gl/Ww5jaP) {{{2
    let $TMPDIR = get(filter(['/run/shm', '/dev/shm'], 'isdirectory(v:val)'), 0, $TMPDIR)
    if $XMODIFIERS is# '@im=fcitx'
      source <sfile>:h/macros/vimrc.fcitx
    endif " 2}}}
  endif
endif

" key-mappings {{{1
" <C-u> と際に UNDO のためのポイントを追加する
inoremap <C-u> <C-g>u<C-u>

" Leader/LocalLeader の設定 {{{2
let g:mapleader = "\<Space>" " <Leader> を <Space> に割当て
noremap <Leader>  <Nop>

let g:maplocalleader = "\<S-Space>" " <LocalLeader> を <C-Space> に割当て
noremap <LocalLeader>  <Nop>

" screen / tmux
nnoremap <C-z>  <Nop>
nnoremap <C-z><C-z>  <C-z>

" <C-o>/C-t> で wildmode 中の <Down>/<Up> {{{2
" cnoremap <C-g>  <Space><BS>
cnoremap <expr> <C-g>  <SID>key_down()
function! s:key_down() abort "{{{
  call feedkeys("\<Down>", 't')
  return ''
endfunction "}}}

" count 分だけディレクトリを上に辿ってlcdする {{{2
nnoremap <silent> <C-h>
      \ :<C-u>lcd <C-r>=fnameescape(expand('%:p' . repeat(':h', v:count + 1)))<CR><CR>

" * → i_CTRL-R_/ 時に、\V,\<,\> を除去 {{{2
noremap! <expr> <C-r>/
      \ getcmdtype() =~# '[/?]' ? getreg('/') : substitute(getreg('/'), '\v\\[V<>]', '', 'g')

" コマンドラインの履歴から入力にマッチしたものを呼び出す {{{2
cnoremap <C-x><C-p>  <Up>
cnoremap <C-x><C-n>  <Down>

" Square bracket mapping {{{2
nnoremap ]q  :<C-u>cnext<CR>
nnoremap ]Q  :<C-u>clast<CR>
nnoremap [q  :<C-u>cprevious<CR>
nnoremap [Q  :<C-u>crewind<CR>
nnoremap ]f  :<C-u>cnfile<CR>
nnoremap [f  :<C-u>cpfile<CR>
nnoremap ]n :<C-u>next<CR>
nnoremap ]N :<C-u>last<CR>
nnoremap [n :<C-u>previous<CR>
nnoremap [N :<C-u>rewind<CR>

nnoremap ]l  :<C-u>lnext<CR>
nnoremap ]L  :<C-u>llast<CR>
nnoremap [l  :<C-u>lprevious<CR>
nnoremap [L  :<C-u>lrewind<CR>
nnoremap ]F  :<C-u>lnfile<CR>
nnoremap [F  :<C-u>lpfile<CR>

" user-commands {{{1
" macors/ 以下のファイルを簡単にロードする {{{2
command! -nargs=1 -complete=customlist,s:macro_complete Macro  call s:source_macro(<q-args>)
function! s:source_macro(macro) abort "{{{
  let fname = findfile('macros/' . a:macro . '.vim', &runtimepath)
  if filereadable(fname)
    execute 'source' fname
  endif
endfunction "}}}
function! s:macro_complete(arglead, cmdline, cursorpos) abort "{{{
  return map(globpath(&runtimepath, 'macros/' . a:arglead . '*.vim', 1, 1), 'fnamemodify(v:val,'':t:r'')')
endfunction "}}}

" Unicode の間接参照をデコードする {{{2
command! -nargs=0 -range UnicodeRefDecode
      \ <line1>,<line2>substitute/&#\(x\)\?\(\x\+\);/\=nr2char((!empty(submatch(1))? '0x' : '' ) . submatch(2), 1)/g
command! -nargs=0 -range UnicodeRefDecode
      \ <line1>,<line2>substitute/&#\%(\(\d\{4}\)\|x\(\x\{4}\)\);/\=nr2char(!empty(submatch(1)) ? submatch(1) : '0x'.submatch(2), 1)/g

" フォルダ等を開く {{{2
command! -nargs=? -complete=file Open
      \ call vital#of('vimrc').import('System.File').open(empty(<q-args>) ? expand("%:p:h") : <q-args>)

" & や $ 変数をダンプ {{{2
command! -nargs=* Dump  call dump#var(<f-args>)

" プラグインテスト用 Vim を起動 {{{2
command! -nargs=* -complete=command -bang TestVimLaunch  call testvim#launch(<bang>0, <q-args>)

" mkdir コマンド {{{2
command! -nargs=1 -complete=dir Mkdir  call mkdir(<q-args>, 'p')

" rm コマンド {{{2
" - 0引数 → [カレントバッファのファイル]
" - 1引数 → <削除対象のファイル>
command! -nargs=* -complete=file Delete  earlier 1f | set nomodified | call fops#rm(empty(<q-args>) ? ['%'] : [<f-args>])

" mv コマンド {{{2
" - 1引数 → [カレントバッファのファイル] <移動先>
" - 2引数 → <移動元> <移動先>
command! -nargs=* -complete=file Move  call fops#f_args('fops#mv', <f-args>)

" autocmds {{{1
augroup vimrc_loading
  autocmd!
  autocmd BufDelete *  call vimrc#autocmd#bufdelete_unset_previewwindow()
  autocmd BufRead,BufNewFile *  call vimrc#autocmd#bufread_lcd_repodir(expand('%:p:h'))
  autocmd BufWriteCmd *[,*]  call vimrc#autocmd#bufwritecmd_avoid_enter_accident(expand('<afile>'))
  autocmd BufWritePost * nested  call vimrc#autocmd#bufwritepost_delete_empty(expand('<afile>'))
  autocmd BufWritePre *  call vimrc#autocmd#bufwrite_mkdir_as_necessary(expand('<afile>:p:h'), v:cmdbang)
  autocmd FileChangedRO *  call vimrc#autocmd#filechangedro(expand("<afile>"))
  autocmd InsertLeave *  call vimrc#autocmd#insertleave_diffupdate()
  autocmd VimLeavePre *  call vimrc#autocmd#vimleavepre_delete_swapfiles()
  autocmd VimLeavePre *  call vimrc#autocmd#vimleavepre_delete_tempfiles()
  autocmd FileType dosbatch  setlocal omnifunc=dosbatch_complete#omnifunc
  " http://... のバッファを curl/wget を使って読み込む
  autocmd BufReadCmd   http://*,file://*,https://*  call http#on_bufreadcmd(expand('<amatch>'))
  autocmd FileReadCmd   http://*,file://*,https://*  call http#on_filereadcmd(expand('<amatch>'))
  " ファイル名補完時にパス区切りをファイルによって '\' → '/' に変更する
  autocmd CompleteDone *  call vimrc#autocmd#filesep_on_completedone()
  " 紛らわしい文字をハイライト
  if exists('##OptionSet')
    autocmd OptionSet buftype,diff,filetype,modifiable,previewwindow,readonly
          \ if !has('vim_starting') | call vimrc#mimic#highlight() | endif
  endif
  autocmd BufEnter,WinEnter *
        \ if line('$') > 1 || len(getline(1)) > 0 | call vimrc#mimic#highlight() | endif
  " spellfile
  autocmd SpellFileMissing * call spellfile#LoadFile(expand('<amatch>'))
  " foldexpr
  autocmd InsertEnter *
        \   if &l:foldmethod is# 'expr'
        \ |   let b:fold_info = [&l:foldmethod, &l:foldexpr]
        \ |   setlocal foldmethod=manual foldexpr&
        \ | endif
  autocmd InsertLeave *
        \   if exists('b:fold_info')
        \ |   let [&l:foldmethod, &l:foldexpr] = b:fold_info
        \ | endif
augroup END

" builtin scripts {{{1
if has('vim_starting')
  if has('clientserver')
    if has('patch-7.4.1674')
      execute 'packadd! editexisting'
    else
      runtime macros/editexisting.vim
    endif
  endif
  if has('patch-7.4.1649')
    execute 'packadd! matchit'
  else
    runtime macros/matchit.vim
  endif
endif

" Disable default plugins
let g:loaded_2html_plugin = 1
let g:loaded_getscriptPlugin = 1
let g:loaded_logiPat = 1
let g:loaded_netrwPlugin = 1
let g:loaded_vimballPlugin = 1
let g:loaded_spellfile_plugin = 1
if has('win32')
  let g:loaded_gzip = 1
  let g:loaded_zipPlugin = 1
  let g:loaded_tarPlugin = 1
endif

" ft-vim-syntax {{{2
let g:vimsyn_embed = 'lP' " l: Lua, P: Python

" pi_paren {{{2
let g:matchparen_timeout = 10 " (msec)

" local plugins {{{1
" insguide {{{2
let g:insguide_default_enable = has('gui_running')
let g:insguide_highlight_multiple_windows = has('gui_running')
let g:insguide_filetype_blacklist = ['help', 'markdown', 'rst', 'text']

" FoldMiss (http://goo.gl/VeHzol) {{{2
nnoremap <silent> z/  :<C-u>call foldmiss#filter(v:count, foldmiss#hlsearch())<CR>
nnoremap <silent> zq  :<C-u>call foldmiss#filter(v:count, foldmiss#quickfix())<CR>

" mimic {{{2
let g:mimic_filetype_blacklists = ['text']

" bundle plugins {{{1

filetype plugin indent on
syntax enable

if !&loadplugins || empty(finddir('default', expand(s:rc_dir . '/pack')))
  finish
endif

" plugin config {{{1
augroup vimrc_bundle_plugin
  autocmd!

  autocmd FileType go packadd vim-go-extra | runtime! ftplugin/go/*.vim
  autocmd FileType json  packadd vison
  autocmd FileType markdown packadd vim-gfm-syntax
  autocmd FileType typescript packadd tscompletejob | runtime! ftplugin/typescript.vim

  if !exists(':packadd')
    function! s:packadd() abort "{{{
      let paths = globpath(&runtimepath, 'pack/*/start/*', 1, 1)
      let afters = globpath(&runtimepath, 'pack/*/start/*/after', 1, 1)
      let &runtiimepath = join(paths + [&runtimepath] + afters, ',')
    endfunction "}}}
    autocmd VimEnter *  call s:packadd()
  endif
augroup END

if has('win32')
  execute 'packadd! vim-csharp'
endif

" altr {{{2
nmap ]a  <Plug>(altr-forward)
nmap [a  <Plug>(altr-back)
function! s:on_init_altr() abort "{{{
  call map([['%', '%.rej', '%.orig'], ['%.master', '%.master.cs'],
        \ ['vimrc', 'gvimrc'], ['en/%.txt', 'doc/%.jax'], ['%.py', '%_test.py']], 'altr#define(v:val)')
endfunction "}}}
Defer s:on_init_altr()

" ambicmd {{{2
cnoremap <expr> <Space> getcmdtype() is# ':' ? ambicmd#expand("\<Space>") : "\<Space>"
cnoremap <expr> <CR>    getcmdtype() is# ':' ? ambicmd#expand("\<CR>") : "\<CR>"
autocmd vimrc_bundle_plugin CmdwinEnter *
      \   inoremap <buffer> <expr> <Space> ambicmd#expand("\<Space>")
      \ | inoremap <buffer> <expr> <CR>    ambicmd#expand("\<CR>")

" autofmt {{{2
autocmd vimrc_bundle_plugin FileType txt,help,markdown,rst  setlocal formatexpr=autofmt#japanese#formatexpr()
let g:autofmt_allow_over_tw = 1 " 全角文字ぶら下がりで1カラムはみ出すのを許可

" chbuf {{{2
nnoremap <silent> <C-n>  :<C-u>call chbuf#change_mixed('')<CR>
function! s:chbuf_on_context() abort "{{{
  if !empty(finddir('.git', '.;')) && executable('git')
    return chbuf#external('relative', ['git', 'ls-files', '--no-empty-directory'])
  endif

  if executable('files')
    return chbuf#external('path', ['files', '-a', '-A', '.'], 500)
  endif
  call chbuf#change_current('')
endfunction "}}}
nnoremap <silent> <C-p>  :<C-u>call <SID>chbuf_on_context()<CR>

" completor {{{2
inoremap <silent> <C-b>  <C-r>={-> execute('packadd completor.vim <Bar> set shortmess+=c')}()<CR>

" cshtml {{{2
autocmd vimrc_bundle_plugin BufRead,BufNewFile *.cshtml  setfiletype cshtml

" diff {{{2
set diffexpr=gitdiff#histogramdiffexpr() " diff#histogramdiffexpr()

" sign-deferred {{{2
augroup plugin-sign-deferred
  autocmd!
  autocmd CursorHold *  for winnr in range(1, winnr('$'))
        \ | call sign_deferred#start(winbufnr(winnr))
        \ | endfor
augroup END

" editorconfig {{{2
let g:editorconfig_root_chdir = 1
augroup plugin-editorconfig
  autocmd!
  autocmd VimEnter * nested
        \ if !argc() && !empty(expand('%')) | call editorconfig#load() | endif
  autocmd BufNewFile,BufReadPost * nested
        \ if isdirectory(expand('%:p:h')) | call editorconfig#load() | endif
augroup END


" emmet {{{2
autocmd vimrc_bundle_plugin FileType css,haml,html,less,sass,scss,slim packadd emmet-vim | EmmetInstall
let g:user_emmet_leader_key = '<LocalLeader>'
let g:user_emmet_install_global = 0

" gf-user {{{2
autocmd vimrc_bundle_plugin FileType vim
      \   call gf#user#extend('vimrc#gf#runtime_find', 1000)
      \ | call gf#user#extend('vimrc#gf#source_find', 1000)
autocmd vimrc_bundle_plugin FileType delphi,pascal
      \ call gf#user#extend('vimrc#gf#pascal_find', 1000)

" guess {{{2
" Windowsでエンコーディングの違いによる文字化けを回避
if &encoding != &termencoding
  autocmd vimrc_bundle_plugin QuickfixCmdPost make  call s:fix_encoding()
  if &grepprg !~# '^jvgrep'
    autocmd vimrc_bundle_plugin QuickfixCmdPost grep  call s:fix_encoding()
  endif
  function! s:fix_encoding() "{{{
    call setqflist(map(getqflist(), 'extend(v:val, {''text'': iconv(v:val.text, ''guess#guess(v:val.text)'', &encoding)})'), 'r')
  endfunction "}}}
endif

" inline_edit {{{2
let g:inline_edit_patterns = get(g:, 'inline_edit_patterns', [])
let g:inline_edit_patterns +=
      \ [ { 'main_filetype': 'rst'
      \   , 'callback': 'vimrc#inline_edit#rst_fenced_code'}
      \ , { 'main_filetype': 'asciidoc'
      \   , 'callback': 'vimrc#inline_edit#asciidoc_fenced_code'}
      \ , { 'main_filetype': 'aspx'
      \   , 'sub_filetype': 'javascript'
      \   , 'indent_adjustment': 2
      \   , 'start': '<script\>[^>]*>'
      \   , 'end': '</script>'}
      \ , { 'main_filetype': 'iss'
      \   , 'sub_filetype': 'pascal'
      \   , 'callback': 'vimrc#inline_edit#iss_fenced_code'}
      \ ]

" jasegment {{{2
let g:jasegment#model = has('win32') ? 'knbc_bunsetu'
      \ : vimrc#executable('cabocha') ? 'cabocha'
      \   : vimrc#executable('mecab') ? 'mecab' : 'knbc_bunsetu'

" markdown {{{2
let g:markdown_fenced_languages = ['vim', 'cs', 'sh', 'dosini', 'python', 'javascript', 'dosbatch', 'xml']

" openbrowser {{{2
let g:openbrowser_no_default_menus = 1
nmap gx  <Plug>(openbrowser-smart-search)
xmap gx  <Plug>(openbrowser-smart-search)

" operator-user {{{2
nmap gc  <Plug>(operator-comment)
xmap gc  <Plug>(operator-comment)
nmap gC  <Plug>(operator-uncomment)
xmap gC  <Plug>(operator-uncomment)
nmap gcc  <Plug>(operator-comment)V_
nmap gCC  <Plug>(operator-uncomment)V_

nmap <silent> s  <Plug>(operator-siege-add)
xmap <silent> s  <Plug>(operator-siege-add)
nmap <silent> ds  <Plug>(operator-siege-delete)
nmap <silent> cs  <Plug>(operator-siege-change)

nmap gr  <Plug>(operator-replace)
xmap gr  <Plug>(operator-replace)
" {{{3 misc

" put markers with one whitespace prefix
nmap zf <Plug>(operator-better-zf)
xmap zf <Plug>(operator-better-zf)

" eval vim variables
nmap gy  <Plug>(operator-eval)
xmap gy  <Plug>(operator-eval)

nmap g:  <Plug>(operator-excmd)
xmap g:  <Plug>(operator-excmd)

nmap g<Space>  <Plug>(operator-trailing-space)
xmap g<Space>  <Plug>(operator-trailing-space)

function! s:on_init_operator() abort "{{{
  call vimrc#operator#init()
endfunction "}}}
Defer s:on_init_operator()

" python-syntax {{{2
let g:python_highlight_all = 1
let g:python_slow_sync = 0

" quickrun {{{2
map <Leader>r <Plug>(quickrun)
command! -nargs=+ -complete=command Capture  QuickRun -type vim -src <args>
let g:quickrun_config = get(g:, 'quickrun_config', {})
let g:quickrun_config._ = {
      \   'outputter/buffer/close_on_empty': 1
      \ , 'hook/repeat_last/enable' : 1
      \ }
if &shelltemp && has('win32')
  let g:quickrun_config._['hook/output_encode/encoding'] = &termencoding . ':' . &encoding
endif

if has('clientserver')
  let g:quickrun_config._.runner = 'remote'
endif

" Use LinqPad if available {{{3
autocmd vimrc_bundle_plugin FileType cs  call vimrc#quickrun#cs_with_linqpad()
" Setup Visual C++ {{{3
autocmd vimrc_bundle_plugin FileType cpp  call vimrc#quickrun#cpp_with_visualstudio()

" restart {{{2
let g:restart_sessionoptions = 'curdir,folds,resize,slash,tabpages,winpos'

" sonictemplate {{{2
let g:sonictemplate_vim_template_dir = expand(s:rc_dir . '/template')
let g:sonictemplate_key = '<LocalLeader>t'
let g:sonictemplate_intelligent_key = '<LocalLeader>T'
let g:sonictemplate_postfix_key = '<LocalLeader><LocalLeader>'
execute 'nmap' g:sonictemplate_key '<plug>(sonictemplate)'
execute 'imap' g:sonictemplate_key '<plug>(sonictemplate)'
execute 'nmap' g:sonictemplate_intelligent_key '<plug>(sonictemplate-intelligent)'
execute 'imap' g:sonictemplate_intelligent_key '<plug>(sonictemplate-intelligent)'
execute 'imap' g:sonictemplate_postfix_key '<plug>(sonictemplate-postfix)'

" textobj-user {{{2
let g:textobj_between_no_default_key_mappings = 1
omap im  <Plug>(textobj-between-i)
xmap im  <Plug>(textobj-between-i)
omap am  <Plug>(textobj-between-a)
xmap am  <Plug>(textobj-between-a)

function! s:textobj_fold() abort "{{{
  execute 'packadd vim-textobj-fold'
endfunction "}}}
Defer s:textobj_fold()

function! s:textobj_entire() abort "{{{
  execute 'packadd vim-textobj-entire'
endfunction "}}}
Defer s:textobj_entire()

" vison {{{2
autocmd BufNewFile,BufRead .bowerrc
      \   setfiletype json | silent! Vison bowerrc.json
autocmd BufNewFile,BufRead .eslint,.eslintrc.json
      \   setfiletype json | silent! Vison eslintrc.json
autocmd BufNewFile,BufRead .csslintrc
      \   setfiletype json | silent! Vison csslintrc.json

" ya-vimomni {{{2
let g:yavimomni_enable_autoload_functions = 1

" YouCompleteMe {{{2
" if paq#available('Valloric/YouCompleteMe')
"   if !get(g:, 'loaded_youcompleteme', 0)
"     command! YouCompleteMe
"           \   delcommand YouCompleteMe
"           \ | PaqEnable Valloric/YouCompleteMe
"           \ | call youcompleteme#Enable()
"   endif
" endif

" finalize {{{1
delcommand Defer
" 1}}}
