" Vim compiler file
" Compiler:     IAR EWARM
" Maintainer:   sgur <sgurrr@gmail.com>
" Last Change:  2012 Apr 30

if exists("current_compiler")
  finish
endif
let current_compiler = "iar"
let s:keepcpo= &cpo
set cpo&vim

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet errorformat=
      \%f(%l)\ :\ %t%*[^[]%m,
      \%-G%.%#


let &cpo = s:keepcpo
unlet s:keepcpo
