" Vim compiler file
" Compiler:     vint
" Maintainer:   sgur
" Last Change:  2015/8/27

if exists('current_compiler')
  finish
endif
let current_compiler = 'vint'

if exists(':CompilerSet') != 2
  command -nargs=* CompilerSet setlocal <args>
endif

let s:save_cpo = &cpo
set cpo&vim

CompilerSet makeprg=vint

CompilerSet errorformat=
      \%f:%l:%c:\ %m

let &cpo = s:save_cpo
unlet s:save_cpo
