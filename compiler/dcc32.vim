" Vim compiler file
" Compiler:	dcc32 - Delphi Compiler
" Maintainer:	sgur <sgurrr@gmail.com>
" Last Change:	2013 Apr 22

if exists("current_compiler")
  finish
endif
let current_compiler = "dcc32"
let s:keepcpo= &cpo
set cpo&vim

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet errorformat=
      \%*\\r%f(%l)%*[^:]:\ %t%n\ %m,
      \%-G%.%#

CompilerSet makeprg=dcc32\ -Q\ %

let &cpo = s:keepcpo
unlet s:keepcpo
