" Vim compiler file
" Compiler:     ccrx (Renesas C compiler)
" Maintainer:   sgur <sgurrr@gmail.com>
" Last Change:  2012 Apr 30
"
if exists("current_compiler")
  finish
endif
let current_compiler = "ccrx"

let s:cpo_save = &cpo
set cpo-=C

CompilerSet errorformat=
  \%f(%l)\ :\ C%n\ (%t)\ %m,
  \%f(%l)\ :\ C%n\ (%t)\ %m,
  \\*\*\ L%n\ (%t)\ %m,
  \%-G%.%#

CompilerSet makeprg=Hmake.exe

let &cpo = s:cpo_save
unlet s:cpo_save
