" if exists("current_compiler")
"   finish
" endif
let current_compiler = "winddk"

if exists(":CompilerSet") != 2
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet makeprg=build

CompilerSet errorformat=
  \%[0-9]>%f(%l)\ :\ %trror\ C%n:\ %m,
  \%[0-9]>%f(%l)\ :\ %tarning\ C%n:\ %m,
  \%-G%.%#
