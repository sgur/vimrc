scriptencoding utf-8
" 独自の filetype ファイル
if exists('did_load_filetypes')
  finish
endif
augroup filetypedetect
  autocmd BufNewFile,BufRead *.vssettings  setfiletype xml
  autocmd BufNewFile,BufRead *.git/TAG_EDITMSG  setfiletype gitcommit
  autocmd BufNewFile,BufRead *.isl  setfiletype dosini
  autocmd BufNewFile,BufRead *.als  setfiletype alloy4
  autocmd BufNewFile,BufRead *.txt,*.text,README  setfiletype text | setlocal syntax=hybrid
  autocmd BufNewFile,BufRead *.csx  setfiletype cs
augroup END
