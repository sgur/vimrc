" pack
" Version: 0.0.1
" Author: 
" License: 

if exists('g:loaded_pack')
  finish
endif
let g:loaded_pack = 1

let s:save_cpo = &cpo
set cpo&vim

augroup plugin_pack
  autocmd!
  autocmd FileType *  call s:on_filetype(expand('<amatch>'))
augroup END

let s:basedir = expand('<sfile>:p:h:h') . '/pack'
let s:loaded = {}


function! s:on_filetype(filetype) abort "{{{
  if !exists('s:json')
    let s:json = s:get_sources_json(s:basedir . '/sources.json')
  endif
  for categeory in values(map(filter(deepcopy(s:json), 'has_key(v:val, ''opt'')'), 'v:val.opt'))
    for [bundle, option] in items(categeory)
      if !has_key(option, 'filetype') || has_key(s:loaded, bundle)
        continue
      endif
      if !empty(filter(copy(s:get_filetypes(option)), 'v:val ==? a:filetype'))
        execute 'packadd' fnamemodify(bundle, ':t')
        echomsg 'packadd' fnamemodify(bundle, ':t')
        let s:loaded[bundle] = 1
      endif
    endfor
  endfor
endfunction "}}}

function! s:get_filetypes(option) abort "{{{
  return type(a:option.filetype) == type('') ? [a:option.filetype] : a:option.filetype
endfunction "}}}

function! s:get_sources_json(path) abort "{{{
  return json_decode(join(readfile(expand(a:path))))
endfunction "}}}

let &cpo = s:save_cpo
unlet s:save_cpo

" vim:set et:
