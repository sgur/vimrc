scriptencoding utf-8
" (g)vim -u NONE -N -S macros/update_packages.vim


" Interface {{{1

function! s:cmd_update() abort "{{{
  let json = s:get_sources_json(s:basedir . '/sources.json')
  let more = &more
  try
    call s:setup_window()
    set nomore
    call s:queue_jobs(s:urls(json))
  finally
    call s:teardown_window()
    let &more = more
  endtry
endfunction "}}}


" Internal {{{1

function! s:setup_window() abort "{{{
  let cur_id = win_getid()
  try
    wall
    silent! new +setlocal\ buflisted\ buftype=nofile\ statusline=Updating...
    let s:winnr = win_getid()
    redraw
  finally
    call win_gotoid(cur_id)
  endtry
endfunction "}}}

function! s:teardown_window() abort "{{{
  let cur_id = win_getid()
  try
    let result = win_gotoid(s:winnr)
    if result
      setlocal statusline=Done!
      redraw
    endif
  finally
    call win_gotoid(cur_id)
  endtry
endfunction "}}}

function! s:output_window(messages) abort "{{{
  let cur_id = win_getid()
  try
    let result = win_gotoid(s:winnr)
    if result
      call append(line('$'), a:messages)
      call cursor(line('$'), 0)
      redraw
    endif
  finally
    call win_gotoid(cur_id)
  endtry
endfunction "}}}

function! s:queue_jobs(urls) abort "{{{
  let urls = copy(a:urls)
  let jobs = []

  try
    while 1
      call filter(jobs, 'job_status(v:val) is# "run"')
      let remains = s:jobs_max - len(jobs)

      if remains == 0
        sleep 200m
        continue
      endif

      for i in range(remains)
        if empty(urls)
          break
        endif
        let [target; urls] = urls
        let job = s:update_repo(target.repo, target.path)
        let jobs += [job]
      endfor

      if empty(urls) && empty(jobs)
        break
      endif
    endwhile
  catch /^Vim:Interrupt$/
    echomsg 'Interrupt'
  endtry
endfunction "}}}

function! s:helptags(doc_path) abort "{{{
  let path = expand(a:doc_path . '/doc')
  if isdirectory(path)
    silent execute 'helptags' path
  endif
endfunction "}}}

function! s:update_repo(repo, path) abort "{{{
  let key = fnamemodify(a:repo, ':t')
  if has_key(s:plugins_current, key)
    let s:plugins_current[key] = 1
  endif
  if isdirectory(a:path)
    call s:job_git_remote(a:repo, a:path)
    return s:job_git_update(a:repo, a:path)
  else
    let s:plugins_installed += [a:repo]
    return s:job_git_clone(a:repo, a:path)
  endif
endfunction "}}}

function! s:job_git_remote(repo, path) abort "{{{
  lcd `=a:path`
  let cmd = printf('%s remote get-url origin', s:git_cmd)
  let info = {
        \ 'repo': a:repo,
        \ 'path': a:path
        \ }
  let s:git_remote_callback_end = 0
  let job = job_start(cmd, {
        \ 'out_mode': 'nl', 'err_mode': 'nl',
        \ 'out_cb': function('s:git_remote_callback_on_out', info)
        \ })
  while s:git_remote_callback_end == 0
    sleep 100m
  endwhile
endfunction "}}}

function! s:git_remote_callback_on_out(channel, message) abort dict "{{{
  let url = s:normalize_url(self.repo)
  let remote_url = a:message
  if remote_url isnot# url
    lcd ..
    call delete(self.path, 'rf')
  endif
  let s:git_remote_callback_end = 1
endfunction "}}}

function! s:update_statusline(message, repo) abort "{{{
  let cur_id = win_getid()
  try
    call win_gotoid(s:winnr)
    let &l:statusline = a:message . ' ' . a:repo
    redraw
  finally
    call win_gotoid(cur_id)
  endtry
endfunction "}}}

function! s:job_git_update(repo, path) abort "{{{
  lcd `=a:path`
  call s:update_statusline('Update', a:repo)
  let cmd = printf('%s pull --update-shallow --recurse-submodules=on-demand --jobs=4 --ff-only', s:git_cmd)
  let info = {
        \ 'repo': a:repo,
        \ 'output': []
        \ }
  return job_start(cmd, {
        \ 'out_mode': 'nl', 'err_mode': 'nl', 'err_io': 'out',
        \ 'out_cb': function('s:callback_on_stdout', info), 'close_cb': function('s:git_update_callback_on_close', [a:path], info)
        \ })
endfunction "}}}

function! s:git_update_callback_on_close(path, channel) abort dict "{{{
  " if !empty(get(self, 'output', []))
  "   call call('s:callback_on_close', [a:channel], self)
  " endif

  lcd `=a:path`
  for output in get(self, 'output', [])
    let matches = matchlist(output, '^Updating\s\+\([[:xdigit:]]\+\)\.\.\([[:xdigit:]]\+\)\s*$')
    if !empty(matches)
      let cmd = printf('%s --no-pager log --no-color --oneline %s..%s', s:git_cmd, matches[1], matches[2])
      let info = {
            \ 'repo': self.repo,
            \ 'output': []
            \ }
      " TODO: git pull した後、log を出し、git fetch --depth=1 するべき?
      call job_start(cmd, {
            \ 'out_mode': 'nl', 'err_mode': 'nl', 'err_io': 'out',
            \ 'out_cb': function('s:callback_on_stdout', info), 'close_cb': function('s:callback_on_close', info)
            \ })
    endif
  endfor
endfunction "}}}

function! s:job_git_clone(repo, path) abort "{{{
  call s:update_statusline('Clone', a:repo)
  let url = s:normalize_url(a:repo)
  let cmd = printf('%s clone --depth=1 --recurse-submodules --jobs=4 %s %s', s:git_cmd, url, a:path)
  let info = {
        \ 'repo': a:repo,
        \ 'output': []
        \ }
  return job_start(cmd, {
        \ 'out_mode': 'nl', 'err_mode': 'nl', 'err_io': 'out',
        \ 'out_cb': function('s:callback_on_stdout', info), 'close_cb': function('s:git_clone_callback_on_close', [a:path], info)
        \ })
endfunction "}}}

function! s:callback_on_stdout(channel, message) abort dict "{{{
  let self.output += [a:message]
endfunction "}}}

function! s:git_clone_callback_on_close(path, channel) abort dict "{{{
  call call('s:callback_on_close', [a:channel], self)
  if !s:support_helptags_all
    return
  endif
  call s:helptags(a:path . '/doc')
endfunction "}}}

function! s:callback_on_close(channel) abort dict "{{{
  call s:output_window(['From ' . self.repo . ':'] + self.output)
endfunction "}}}

function! s:normalize_url(repo) abort "{{{
  return a:repo =~ '^https\?://'
        \ ? a:repo
        \ : 'https://github.com/' . a:repo
endfunction "}}}

function! s:get_sources_json(path) abort "{{{
  return json_decode(join(readfile(expand(a:path))))
endfunction "}}}

function! s:urls(json) abort "{{{
  let _ = []
  for [root, packages] in items(a:json)
    for [type, repos] in items(packages)
      for repo in repos
        let dirpath = expand(printf('%s/%s/%s/%s', s:basedir, root, type, fnamemodify(repo, ':t')))
        let _ += [{'repo': repo, 'path': dirpath}]
      endfor
    endfor
  endfor
  return _
endfunction "}}}

function! s:init_states() abort "{{{
  let s:plugins_installed = []
  let s:plugins_current = {}
  call map(globpath('~/.vim/pack', '*/*/*',1,1), {key, val -> extend(s:plugins_current, {fnamemodify(val, ":t"): 0})})
endfunction "}}}


" Initialization {{{1

let s:support_helptags_all = has('patch-7.4.1551')
let s:git_cmd = get(g:, 'git_cmd', 'git')
let s:jobs_max = 8
let s:basedir = expand('<sfile>:p:h:h') . '/pack'

call s:init_states()
call s:cmd_update()

echo filter(s:plugins_current, {key, val -> val == 0})

if !s:support_helptags_all
  helptags ALL
endif

if !empty(s:plugins_installed)
  echomsg 'New plugin has been installed.' '[' . join(s:plugins_installed, ', ') . ']'
  echomsg 'Please restart' v:progname
endif


" 1}}}
