﻿scriptencoding utf-8

let s:save_cpo = &cpo
set cpo&vim



" Internal {{{1

" Return list of line numbers for current buffer found in quickfix list.
function! s:filter_quickfix(qfs) abort "{{{
  return  map(filter(a:qfs, 'v:val.valid && v:val.bufnr == bufnr("%")'), 'v:val.lnum')
endfunction "}}}


" Return list of line numbers in hlsearch.
function! s:filter_hlsearch() abort "{{{
  return filter(range(1, line('$')), 'getline(v:val) =~ @/')
endfunction "}}}


" Add manual fold from line1 to line2, inclusive.
function! s:fold(line1, line2) abort "{{{
  if a:line1 < a:line2
    execute printf('%d,%dfold', a:line1, a:line2)
  endif
endfunction "}}}


function! s:fold_gap(context, lnums) abort "{{{
  let last = -a:context
  for lnum in a:lnums
    call s:fold(last + 1 + a:context, lnum - 1 - a:context)
    let last = lnum
  endfor
  call s:fold(last + 1 + a:context, line('$'))
endfunction "}}}


" Fold non-matched lines.
function! s:foldmiss(context, lnums) abort "{{{
  setlocal foldlevel=0
  setlocal foldmethod=manual
  normal! zE
  call s:fold_gap(a:context, a:lnums)
endfunction  "}}}


" Interface {{{1

function! foldmiss#quickfix() abort
  return s:filter_quickfix(getqflist())
endfunction


function! foldmiss#loclist() abort
  return s:filter_quickfix(getloclist(0))
endfunction


function! foldmiss#hlsearch() abort
  return s:filter_hlsearch()
endfunction


function! foldmiss#filter(context, num) abort
  if empty(get(w:, 'foldmiss', {}))
    if empty(a:num)
      echohl ErrorMsg | echo "Foldmiss: no matches!" | echohl Normal | return
      return
    endif
    let w:foldmiss = {
          \   'foldmethod': &l:foldmethod
          \ , 'foldlevel': &l:foldlevel
          \ }
    call s:foldmiss(a:context, a:num)
    autocmd WinEnter,BufWinEnter <buffer>  if !empty(get(w:, 'foldmiss', {}))
          \ | call s:reset_foldmiss()
          \ | endif
  else
    call s:reset_foldmiss()
  endif
endfunction

function! s:reset_foldmiss() abort "{{{
    let &l:foldmethod = w:foldmiss.foldmethod
    let &l:foldlevel = w:foldmiss.foldlevel
    unlet w:foldmiss
endfunction "}}}


let &cpo = s:save_cpo
unlet s:save_cpo
