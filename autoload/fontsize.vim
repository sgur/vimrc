scriptencoding utf-8
"


" Interface {{{1

function! fontsize#set(size, reset) abort
  if empty(a:size) | echo s:info() | return | endif
  if a:reset | call s:reset() | return | endif

  call s:keep()
  call s:fontsize(a:size)
endfunction


" Internal {{{1

function! s:reset() abort "{{{
  if exists('s:keep')
    let [&guifont, &guifontwide, &lines, &columns] = s:keep
    unlet! s:keep
    return
  endif
endfunction "}}}

function! s:keep() abort "{{{
  if !exists('s:keep')
    let s:keep = [&guifont, &guifontwide, &lines, &columns]
  endif
endfunction "}}}

function! s:info() abort "{{{
  let _ = []
  let _ += ['guifont=' . &guifont]
  if !empty(&guifont)
    let _ += ['guifontwide=' . &guifontwide]
  endif
  return join(_, "\n")
endfunction "}}}

function! s:change_fontsize(font, size) abort "{{{
  return join(map(split(a:font, '\\\@<!,\s*'),
        \ printf('substitute(v:val, %s, %s, "g")',
        \ string(s:fontsize_pattern()),
        \ string('\=sort([1,' . a:size . '], "n")[-1]'))), ',')
  " return join(map(split(a:font, '\\\@<!,\s*')
  "       \ { key, val -> substitute(val, }), ',')
endfunction "}}}

function! s:fontsize(size) abort "{{{
  let step = get(g:, 'fontsize_step', 0.5)
  let newsize = a:size =~# '^[+-]' ? 'str2float(submatch(0))' . a:size . '*' . string(step) : a:size
  let &guifont = s:change_fontsize(&guifont, newsize)
  let &guifontwide = s:change_fontsize(&guifontwide, newsize)
  " Keep window size if possible.
  let [&lines, &columns] = s:keep[2 :]
endfunction "}}}

function! s:guifont_pattern() abort "{{{
  if has('win32') || has('win64') || has('mac') || has('macunix')
    return 'windows'
  elseif has('gui_gtk') || has('gui_qt')
    return 'gtk'
  elseif has('X11')
    return 'x11'
  else
    echoerr 'Fontsize: Unknown platform'
  endif
endfunction "}}}

function! s:fontsize_pattern() abort "{{{
  return get(g:, 'fontsize_pattern', s:guifont_{s:guifont_pattern()}_pattern)
endfunction "}}}


" Initialization {{{1

let s:guifont_windows_pattern = ':h\zs\d\+\%(\.\d\+\)\?'
let s:guifont_gtk_pattern = '\s\+\zs[[:digit:].]\+$'
let s:guifont_x11_pattern = '\v%([^-]*-){6}\zs[[:digit:].]\ze%(-[^-]*){7}'


" 1}}}
