scriptencoding utf-8



" Interface {{{1


" Internal {{{1

function! s:glob_shellcmds() abort "{{{
  let raw_globbed = has('win32')
        \ ? globpath(join(split($PATH, ';'), ','), '*.exe', 1, 1)
        \ : globpath(join(split($PATH, ':'), ','), '*',     1, 1)
  let sub_pattern = has('win32')
        \ ? ':t:gs?\.exe??'
        \ : ':t'
  return uniq(sort(map(filter(raw_globbed, 'executable(v:val)'), 'fnamemodify(v:val, sub_pattern)')))
endfunction "}}}

function! s:setbufvars(bufnr) abort "{{{
  call setbufvar(a:bufnr, '&buftype', 'nofile')
  call setbufvar(a:bufnr, '&buflisted', 0)
  call setbufvar(a:bufnr, '&bufhidden', 'delete')
  call setbufvar(a:bufnr, '&previewwindow', 1)
endfunction "}}}

function! s:decode_content(content) abort "{{{
  try
    return {exists('*json_decode') ? 'json_decode' : 'webapi#json#decode'}(a:content)
  catch /^Vim\%((\a\+)\)\=:E117/
    throw 'webapi-vim required'
  endtry
  return ''
endfunction "}}}
 
function! s:result(path) abort "{{{
  let url = printf(s:url_format, a:path, 'json')
  try
    let result = webapi#http#get(url)
  catch /^Vim\%((\a\+)\)\=:E117/
    throw 'webapi-vim required'
  endtry
  return result.status is# '200' ? s:decode_content(result.content) : []
endfunction "}}}

function! s:using(cmd) abort "{{{
  return printf('using/%s', a:cmd)
endfunction "}}}

function! s:matching(term) abort "{{{
  try
    let encoded = webapi#base64#b64encode(a:term)
    return printf('matching/%s/%s', a:term, encoded)
  catch /^Vim\%((\a\+)\)\=:E117/
    throw 'webapi-vim required'
  endtry
  return ''
endfunction "}}}

function! s:quickfix_using(cmd) abort "{{{
  let path = s:using(a:cmd)
  try
    let result = s:result(path)
    call setqflist(map(result, "{
          \   'filename': printf(s:url_format, path, 'plaintext')
          \ , 'text': v:val.command
          \ , 'pattern': v:val.summary
          \ , 'col': 1
          \ }"))
    copen
  catch
    echohl ErrorMsg | echomsg 'CommandlineFu(quickfix): ' v:exception | echohl NONE
  endtry
endfunction "}}}

function! s:quickfix_matching(term) abort "{{{
  try
    let path = s:matching(a:term)
  catch
    echohl ErrorMsg | echomsg 'CommandlineFu(matching):' v:exception | echohl NONE
  endtry
  call setqflist(map(s:result(path), "{
        \   'filename': printf(s:url_format, path, 'plaintext')
        \ , 'text': v:val.command
        \ , 'pattern': v:val.summary
        \ , 'col': 1
        \ }"))
  copen
endfunction "}}}

function! s:preview_using(cmd) abort "{{{
  let bufname = printf(s:url_format, s:using(a:cmd), 'plaintext')
  silent! execute 'pedit'
        \ '+/' . escape(a:cmd, ' ')
        \ bufname
  call s:setbufvars(bufnr(bufname))
endfunction "}}}

function! s:preview_matching(term) abort "{{{
  try
    let bufname = printf(s:url_format, s:matching(a:term), 'plaintext')
  catch
    echohl ErrorMsg | echomsg 'CommandlineFu(matching):' v:exception | echohl NONE
    return
  endtry
  silent! execute 'pedit'
        \ '+/' . escape(a:term, ' ')
        \ bufname
  call s:setbufvars(bufnr(bufname))
endfunction "}}}

function! s:parse_arguments(arglist) abort "{{{
  let arglist = copy(a:arglist)
  let opt_quickfix = index(arglist, '-quickfix')
  let opt_previewwindow = index(arglist, '-previewwindow')
  let opt_matching = index(arglist, '-matching')
  let opt_using = index(arglist, '-using')
  call map(filter([opt_previewwindow, opt_quickfix, opt_using, opt_matching], 'v:val != -1'), 'remove(arglist, v:val)')
  return [opt_previewwindow == -1 && opt_quickfix != -1 ? 'quickfix' : 'preview'
        \ , opt_matching == -1 && opt_using != -1 ? 'using' : 'matching'
        \ , arglist]
endfunction "}}}

function! commandlinefu#find(...) abort
  let [output, method, arglist] = s:parse_arguments(a:000)
  if !len(arglist)
    echoerr 'CmdFu: Argument required'
    return
  endif
  call s:{output}_{method}(arglist[0])
endfunction

function! commandlinefu#complete(arglead, cmdline, cursorpos) abort
  if !exists('s:shellcmds')
    let s:shellcmds = s:glob_shellcmds()
  endif
  let options = (a:cmdline =~# '-\%(quickfix\|previewwindow\)' ? [] : ['-previewwindow', '-quickfix'])
        \  + (a:cmdline =~# '-\%(using\|matching\)' ? [] : ['-using', '-matching'])
  return filter(options + s:shellcmds, 'stridx(v:val, a:arglead) == 0')
endfunction


" Initialization {{{1

let s:url_format = 'http://www.commandlinefu.com/commands/%s/%s'



" 1}}}
