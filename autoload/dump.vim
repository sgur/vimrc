scriptencoding utf-8


function! dump#var(var, ...) abort
  try
    let values = eval(a:var)
  catch /^Vim\%((\a\+)\)\=:E\%(15\|121\)/
    let values = eval('&' . a:var)
  endtry

  echo join(type(values) == type({})
        \ ? map(items(values), 'v:val[0] . ":\n  " . string(v:val[1])')
        \ : type(values) != type([])
        \   ? get(filter(map(a:000 + [',', ';'], 'split(values, v:val)'), 'len(v:val) > 1'), 0, [values])
        \   : values
        \ , "\n")
endfunction
