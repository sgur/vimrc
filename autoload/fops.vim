" autoload/fops.vim
" File operations

let s:save_cpo = &cpo
set cpo&vim

let s:V = vital#of('vimrc').load('System.File')

function! fops#f_args(function, ...)
  if a:0 == 0
    let [src, dest] = [expand('%'), input('Dest:', expand('%'), 'file')]
    if match(dest, '[\\/]') < 0
      let dest = fnamemodify(dest, ':.')
    endif
  elseif a:0 == 1
    let [src, dest] = [expand('%'), a:1]
    if match(dest, '[\\/]') < 0
      let dest = fnamemodify(dest, ':.')
    endif
  elseif a:0 == 2
    let [src, dest] = [a:1, a:2]
  else
    return
  endif
  if exists('*'.a:function)
    call call(a:function, [src, dest])
  endif
endfunction

function! fops#mv(src, dest) abort
  echomsg a:src a:dest
  let [src, dest] = map([a:src, a:dest], 'substitute(expand(v:val), "\\", "/", "g")')
  if isdirectory(dest)
    let dest .= '/' . fnamemodify(src, ':t')
  endif
  if !s:confirm('Move', join([src, '->', dest], ' '))
    return
  endif
  let dest_dir = fnamemodify(dest, ':p:h')
  if !isdirectory(dest_dir)
    call mkdir(dest_dir, 'p')
  endif
  try
    let result = s:V.System.File.move(src, dest)
    if !result
      echoerr 'Moving' src 'failed.'
    endif
  endtry
  if fnamemodify(bufname('%'), ':p') == fnamemodify(src, ':p')
    let bufnr = bufnr('%')
    execute 'keepalt edit' dest
    execute 'bdelete' bufnr
  endif
endfunction

function! fops#rm(files) abort
  call map(map(a:files, 'expand(v:val, 1)'), 's:rm(v:val)')
endfunction

function! s:rm(path)
  let path = substitute(a:path , '\\', '/', 'g')
  if !s:confirm('Delete', path)
    return
  endif
  if expand('%:p') == fnamemodify(a:path, ':p')
    if bufnr('#') > -1 && !empty(bufname('#'))
      edit #
    else
      enew
    endif
    execute 'bwipeout' a:path
  endif
  if isdirectory(path)
    call s:rmdir(path)
  elseif delete(expand(path))
    echohl ErrorMsg | echomsg 'Deleting' path 'failed.' | echohl NONE
  endif
endfunction

function! s:rmdir(path) "{{{
    let cwds = {}
    bufdo call extend(cwds, {bufnr('%'): getcwd()})
    let is = &fileignorecase ? 'is?' : 'is#'
    call filter(cwds, join(map(['v:val', 'a:path'], 'printf("fnamemodify(%s, \":p\")", v:val)'), is))
    let cwd = getcwd()
    try
      for [nr, dir] in items(cwds)
        execute 'buffer' nr
        echomsg dir
        lcd /
      endfor
    finally
      execute 'lcd' cwd
    endtry
    try
      call s:V.System.File.rmdir(fnamemodify(a:path, ':p:h'), 'r')
    catch /.*/
      echohl ErrorMsg
      echomsg 'Deleting' a:path 'failed.'
      echomsg v:exception
      echohl NONE
    endtry
endfunction "}}}

function! s:confirm(operation, path)
  return confirm(printf('%s: %s ?', a:operation, a:path), "&Yes\n&No", 2) == 1
endfunction

function! s:shellexpand(string)
  return substitute(expand(a:string), '/', '\', 'g')
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo

