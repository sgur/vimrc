scriptencoding utf-8

function! testvim#launch(is_norc, args)
  let rtp_path = tr(getcwd(), '\', '/')
  let cmds_rtp = [printf('runtimepath^=%s', fnameescape(rtp_path))]
  let after_path = tr(fnamemodify(finddir('after', rtp_path), ':p:h'), '\', '/')
  let cmds_rtp += after_path isnot# getcwd() ? [printf('runtimepath+=%s', fnameescape(after_path))] : []
  let cmd = printf('%s %s --noplugin -i NONE -N %s --cmd "runtime ftdetect/* plugin/* after/plugin/*" %s'
        \ , has('gui_macvim') ? 'mvim' : v:progname
        \ , a:is_norc ? '-u NORC' : ''
        \ , printf('--cmd "set %s"', join(cmds_rtp))
        \ , !empty(a:args) ? printf('-c "%s"', a:args) : '')
  echomsg cmd
  call vital#of('vimrc').import('Process').spawn(cmd)
endfunction

