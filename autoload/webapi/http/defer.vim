" webapi#http と job を利用して非同期(遅延)で何か処理を行うサンプル
" 
scriptencoding utf-8



" Interface {{{1

function! webapi#http#defer#get(url, callback, ...) abort
  let getdata = a:0 > 0 ? a:000[0] : {}
  let headdata = a:0 > 1 ? a:000[1] : {}
  let follow = a:0 > 2 ? a:000[2] : 1
  let url = a:url
  let getdatastr = webapi#http#encodeURI(getdata)
  if strlen(getdatastr)
    let url .= "?" . getdatastr
  endif
  if executable('curl')
    let command = s:curl_cmd(url, headdata, follow)
  elseif executable('wget')
    let command = s:wget_cmd(url, headdata, follow)
  else
    throw "require `curl` or `wget` command"
    return
  endif

  let info = {
        \ 'buffer': '',
        \ 'follow': follow,
        \ 'callback': a:callback
        \ }
  let job = job_start(command, {
        \ 'out_mode': 'nl',
        \ 'err_mode': 'nl',
        \ 'err_cb': function('s:callback_on_err', [], info),
        \ 'out_cb': function('s:callback_on_out', [], info),
        \ 'close_cb': function('s:callback_on_close', [], info)
        \ })
endfunction

" Internal {{{1

function! s:curl_cmd(url, headdata, follow) abort "{{{
  let command = printf('curl -q %s -s -k -i', a:follow ? '-L' : '')
  let quote = &shellxquote ==# '"' ?  "'" : '"'
  for key in keys(a:headdata)
    if has('win32')
      let command .= " -H " . quote . key . ": " . substitute(a:headdata[key], '"', '"""', 'g') . quote
    else
      let command .= " -H " . quote . key . ": " . a:headdata[key] . quote
    endif
  endfor
  return command . " " . quote . a:url . quote
endfunction "}}}

function! s:wget_cmd(url, headdata, follow) abort "{{{
  let command = printf('wget -O- --save-headers --server-response -q %s', a:follow ? '-L' : '')
  let quote = &shellxquote ==# '"' ?  "'" : '"'
  for key in keys(a:headdata)
    if has('win32')
      let command .= " --header=" . quote . key . ": " . substitute(a:headdata[key], '"', '"""', 'g') . quote
    else
      let command .= " --header=" . quote . key . ": " . a:headdata[key] . quote
    endif
  endfor
  return command . " " . quote . a:url . quote
endfunction "}}}

function! s:callback_on_out(channel, message) dict abort "{{{
  let self.buffer .= a:message . "\n"
endfunction

function! s:callback_on_err(channel, message) dict abort "{{{
  echoerr a:message
  let self.buffer .= a:message . "\n"
endfunction

function! s:callback_on_close(channel) dict abort "{{{
  let follow = get(self, 'follow', 0)
  let res = get(self, 'buffer', '')

  if follow != 0
    while res =~# '^HTTP/\%(1\.[01]\|2\%(\.0\)\?\) 3' || res =~# '^HTTP/1\.[01] [0-9]\{3} .\+\n\r\?\nHTTP/1\.[01] [0-9]\{3} .\+'
      let pos = stridx(res, "\r\n\r\n")
      if pos != -1
        let res = strpart(res, pos+4)
      else
        let pos = stridx(res, "\n\n")
        let res = strpart(res, pos+2)
      endif
    endwhile
  endif
  let pos = stridx(res, "\r\n\r\n")
  if pos != -1
    let content = strpart(res, pos+4)
  else
    let pos = stridx(res, "\n\n")
    let content = strpart(res, pos+2)
  endif
  let header = split(res[:pos-1], '\r\?\n')
  let matched = matchlist(get(header, 0), '^HTTP/\%(1\.[01]\|2\%(\.0\)\?\)\s\+\(\d\+\)\s*\(.*\)')
  if !empty(matched)
    let [status, message] = matched[1 : 2]
    call remove(header, 0)
  else
    if v:shell_error || len(matched)
      let [status, message] = ['500', "Couldn't connect to host"]
    else
      let [status, message] = ['200', 'OK']
    endif
  endif
  call call(self.callback, [{
        \ "status" : status,
        \ "message" : message,
        \ "header" : header,
        \ "content" : content
        \}])
endfunction "}}}

" Initialization {{{1


" 1}}}
