scriptencoding utf-8



" Internal {{{1


" Interface {{{1

function! vimrc#quickrun#cs_with_linqpad() abort
  if !has('win32') | return | endif
  let path = expand('C:/Program Files*/LINQPad*/LPRun.exe', 1)
  if !vimrc#executable(path) | return | endif
  let quickrun_config_lprun =
        \ { 'command': path
        \ , 'exec': '%c:gs?/?\\? %o %s'
        \ , 'tempfile': '%{tempname()}.cs'
        \ , 'hook/output_encode/encoding': '&termencoding'
        \ , 'hook/sweep/files': ['%S:p:r.exe']
        \ }
  let g:quickrun_config['cs/lprun/expression'] = extend({'cmdopt': '-lang=Expression'}, quickrun_config_lprun)
  let g:quickrun_config['cs/lprun/statements'] = extend({'cmdopt': '-lang=Statements'}, quickrun_config_lprun)
  let g:quickrun_config['cs/lprun/program'] = extend({'cmdopt': '-lang=Program'}, quickrun_config_lprun)
  let g:quickrun_config['cs/lprun'] = {'type': 'cs/lprun/statements'}
endfunction

function! vimrc#quickrun#cpp_with_visualstudio() abort
  if !has('win32') | return | endif
  let g:quickrun_config['cpp/msvc2012'] =
        \ { 'command': 'cl'
        \ , 'exec': ['%c %o %s', '%s:p:r.exe %a']
        \ , 'cmdopt': '/EHsc'
        \ , 'hook/output_encode/encoding': 'sjis'
        \ , 'hook/vcvarsall/enable': 1
        \ , 'hook/vcvarsall/bat': shellescape($VS110COMNTOOLS  . '..\..\VC\vcvarsall.bat')
        \ }
endfunction

" Initialization {{{1



" 1}}}
