scriptencoding utf-8



" Interface {{{1

" 最後のVimインスタンスの場合、終了時に swapfile を削除 {{{2
function! vimrc#autocmd#vimleavepre_delete_swapfiles() abort
  let directory = expand(split(&directory, ',')[0])
  if len(split(serverlist(),"\n")) == 1 && isdirectory(directory)
    call map(globpath(directory, '*.sw*', 1, 1), 'delete(v:val)')
  endif
endfunction

" Enter巻き込み事故を防ぐ (http://goo.gl/XxVfPk) {{{2
function! vimrc#autocmd#bufwritecmd_avoid_enter_accident(file) abort
  if !empty(&buftype)
    return
  endif
  let prompt = "Typo: really want to write to '" . a:file . "'?"
  if confirm(prompt, "yes\nno", 2) == 1
    execute 'write' a:file
  endif
endfunction

" 保存時に空ファイルを削除する {{{2
function! vimrc#autocmd#bufwritepost_delete_empty(bufname) abort
  if getfsize(a:bufname) == 0 && a:bufname !~# '__init__\.py$'
    call fops#rm([expand(a:bufname)])
  endif
endfunction

" 保存時に自動的にディレクトリを作成する (http://goo.gl/gPl49j) {{{2
function! vimrc#autocmd#bufwrite_mkdir_as_necessary(dir, force) abort
  if !isdirectory(a:dir) &&
        \ (a:force ||
        \ confirm(printf('"%s" does not exist. Create?', a:dir), "&Yes\n&No", 2) == 1)
    call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
  endif
endfunction

" .git/.hg 以下のときに lcd {{{2
function! vimrc#autocmd#bufread_lcd_repodir(dir) abort
  if a:dir is? fnamemodify(a:dir, ':h')
    return
  endif
  if !empty(
        \ filter(map(['package.json', 'tcsonfig.json'], 'join([a:dir, v:val], ''/'')'), 'filereadable(v:val)') +
        \ filter(map(['.git', '.hg', '$tf'], 'join([a:dir, v:val], ''/'')'), 'isdirectory(v:val) || filereadable(v:val)'))
    lcd `=a:dir`
  else
    call vimrc#autocmd#bufread_lcd_repodir(fnamemodify(a:dir, ':h'))
  endif
endfunction

" ウィンドウ削除時に previewwindow を消去 (http://goo.gl/Su09fx) {{{2
function! vimrc#autocmd#bufdelete_unset_previewwindow() abort
  setlocal nopreviewwindow
endfunction

" InsertLeave 時に &diff == 1 だったら表示を更新する {{{2
function! vimrc#autocmd#insertleave_diffupdate() abort
  if &l:diff
    diffupdate
  endif
endfunction

" Vim 終了時に tempfile を削除する {{{2
function! vimrc#autocmd#vimleavepre_delete_tempfiles() abort
  call map(glob(fnamemodify(tempname(), ':p:h') . '/VI*.tmp', 1, 1), 'delete(v:val)')
endfunction

" TFS 管理下のReadonlyファイルを開いたときに Checkout for edit をする
function! vimrc#autocmd#filechangedro(fname) abort
  if !empty(&buftype)
    return
  endif
  let tf_cmd = s:tf_cmd()
  let tfs_path = fnamemodify(a:fname, ':p')
  if !has('job')
    call system(printf('"%s" vc localversions %s', tf_cmd, tfs_path))
    if !v:shell_error
      call system(printf('"%s" vc checkout %s', tf_cmd, a:fname))
      let &l:readonly = !filewritable(a:fname)
    endif
  else
    call job_start(printf('%s vc localversions %s', tf_cmd, tfs_path), {
          \ 'out_cb': function('s:on_filechangedro_out_cb', [tfs_path]),
          \ 'err_io': 'null'})
  endif
endfunction

function! vimrc#autocmd#filesep_on_completedone() abort
  if !has('win32') || &shellslash
    return
  endif
  if empty(v:completed_item)
    return
  endif
  let word = v:completed_item.word
  if word !~# '^\f\+$' || !s:is_matched(expand('%:t'), &filetype)
    return
  endif
  let start = match(getline('.')[: col('.')-1], '\f\+"\?$')
  if start == -1
    return
  endif
  let term = substitute(getline('.'), printf('\%%>%dc\%%<%dc\\', start, col('.')), '/', 'g')
  call setline(line('.'), term)
endfunction


" Internal {{{1

function! s:is_matched(bufname, filetype) abort "{{{
  return !empty(
        \ filter(copy(g:config#autocmd#filesep_whitelist.pattern), 'a:bufname =~ v:val') +
        \ filter(copy(g:config#autocmd#filesep_whitelist.filetype), 'a:filetype is# v:val'))
endfunction "}}}

function! s:tf_cmd() abort "{{{
  if exists('s:tf_cmd')
    return s:tf_cmd
  endif

  let s:tf_cmd = get(glob('C:/Program Files (x86)/Microsoft Visual Studio */Common7/IDE/TF.exe', 1, 1), 0, '')
  if &shell =~? 'cmd.exe' && &shellslash
    let s:tf_cmd = tr(s:tf_cmd, "'", '"')
  endif
  return s:tf_cmd
endfunction "}}}

function! s:on_filechangedro_out_cb(path, channel, message) abort "{{{
  if a:message !~# '\f\+;C\d\+' || a:path isnot? expand('%:p')
    return
  endif
  call job_start(printf('"%s" vc checkout %s', s:tf_cmd(), a:path), {
        \ 'exit_cb': function('s:tf_checkout_on_exit_cb')})
endfunction "}}}

function! s:tf_checkout_on_exit_cb(job, status) abort "{{{
  setlocal noreadonly
  command! -buffer -nargs=0 TfUndo
        \ call job_start(printf('"%s" vc undo %s', s:tf_cmd(), expand('%')), {
        \  'exit_cb': function('s:tf_undo_on_exit_cb')})
endfunction "}}}

function! s:tf_undo_on_exit_cb(job, status) abort "{{{
  edit!
endfunction "}}}


" Initialization {{{1

let g:config#autocmd#filesep_whitelist = {
      \ 'pattern' : ['^\.\w\+rc$'],
      \ 'filetype' : ['gitconfig']
      \ }


" 1}}}
