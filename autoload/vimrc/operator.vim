scriptencoding utf-8



" Internal {{{1

" Interface {{{1

function! vimrc#operator#zf(motion_wiseness) abort
  if &l:foldmethod is# 'marker'
    call map(map(["'[", "']"], 'line(v:val)'), 'setline(v:val, getline(v:val) . '' '')')
  endif
  '[,']fold
endfunction

function! vimrc#operator#eval(motion_wiseness) abort
  let term = a:motion_wiseness isnot# 'line' ? getline("'[")[col("'[")-1: col("']")-1] : getline("'[")
  try
    execute 'echo' printf('prettyprint#prettyprint(%s)', term)
  catch /^Vim\%((\a\+)\)\=:E121/
    echohl WarningMsg | echomsg v:exception | echohl NONE
  endtry
endfunction

function! vimrc#operator#excmd(motion_wiseness) abort
  normal! '[V']
  redraw
  execute "'[,']" input('excmd? ', '', 'command')
  execute 'normal!' "\<ESC>"
endfunction

function! vimrc#operator#init() abort
endfunction


" Initialization {{{1

call operator#user#define('better-zf', 'vimrc#operator#zf')
call operator#user#define('eval', 'vimrc#operator#eval')
call operator#user#define('excmd', 'vimrc#operator#excmd')
call operator#user#define_ex_command('trailing-space', 'substitute/\s\+$//ge')
call operator#user#define_ex_command('sort', 'sort')


" 1}}}
