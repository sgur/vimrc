scriptencoding utf-8



" Internal {{{1


" Interface {{{1

function! vimrc#inline_edit#rst_fenced_code() abort
  let start_pattern = '^.*\%(sourcecode\|code\%(-block\)\=\)\?::\%(\s\+\(.\+\)\)\?'
  let end_pattern   = '^\n*\_^\S'
  call inline_edit#PushCursor()
  try
    let start = search(start_pattern, 'bcW') " find start of area
    if start == 0 || start == 1 | return [] | endif
    while empty(getline(start+1)) " skip empty line
      let start += 1
    endwhile
    let filetype = get(matchlist(getline('.'), start_pattern), 1, '')
    let end = search(end_pattern, 'W') " find end of area
    if end == 0 || end == line('$') | return [] | endif
    return [start+1, end, filetype, indent(end)]
  finally
    call inline_edit#PopCursor()
  endtry
endfunction

function! vimrc#inline_edit#asciidoc_fenced_code() abort
  let start_pattern = '^\[\s*source,\s*\(\w\+\)\s*\]$'
  let block_pattern = '^-\{4,}$'
  call inline_edit#PushCursor()
  try
    let start = search(start_pattern, 'bcW') " find start of area
    if start == 0 || start == 1 | return [] | endif
    let filetype = get(matchlist(getline('.'), start_pattern), 1, '')
    if getline(start + 1) !~# block_pattern
      return [start+1, start+1, filetype, 0]
    endif
    normal! 2j
    let end = search(block_pattern, 'W') " find end of area
    if end == 0 || end == line('$') | return [] | endif
    return [start+2, end-1, filetype, 0]
  finally
    call inline_edit#PopCursor()
  endtry
endfunction

function! vimrc#inline_edit#iss_fenced_code() abort
  call inline_edit#PushCursor()
  try
    let start = search('^\[Code\]$', 'bcW')
    if start == 0 || start == 1 | return [] | endif
    let end = search('^\[.\+\]', 'W')
    return [start+1, end == 0 ? line('$') : end-1, 'delphi', 0]
  finally
    call inline_edit#PopCursor()
  endtry
endfunction

" Initialization {{{1



" 1}}}
