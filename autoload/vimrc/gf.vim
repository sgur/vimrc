scriptencoding utf-8



" Internal {{{1

function! s:pascal_find_uses_clause(lnum) abort "{{{
  let found_uses = 0
  for l in range(a:lnum, 1, -1)
    let line = getline(l)
    if empty(line)
      return found_uses
    endif
    if stridx(line, 'uses') > -1
      let found_uses = 1
    endif
  endfor
endfunction "}}}

function! s:pascal_find_start(line, cursor_index) abort " {{{
  for i in range(a:cursor_index, 0, -1)
    if a:line[i] !~ '\k'
      return i+1
    endif
  endfor
  return 0
endfunction " }}}


" Interface {{{1

function! vimrc#gf#runtime_find() abort
  if &filetype isnot# 'vim' | return 0 | endif
  let match = matchstr(getline('.'), '\v<runtime>\!?\s*\zs(\f+)\ze\s*')
  if empty(match) | return 0 | endif
  return {'path': findfile(match, &runtimepath), 'line': 1, 'col': 0}
endfunction

function! vimrc#gf#source_find() abort
  if &filetype isnot# 'vim' | return 0 | endif
  let match = matchstr(getline('.'), '\v<source>\!?\s*\zs(\S+)\ze\s*')
  if empty(match) | return 0 | endif
  let fnmod = matchlist(match, '\(<sfile>\)\(\%(:\w\)*\)\(.\+\)')
  return {'path': fnamemodify(substitute(fnmod[1], '<sfile>', '%', ''), fnmod[2]) . fnmod[3], 'line': 1, 'col': 0}
endfunction

function! vimrc#gf#pascal_find() abort
  if &filetype isnot# 'pascal' || !s:pascal_find_uses_clause(line('.'))
    return 0
  endif
  let line = getline('.')
  let start = s:pascal_find_start(line, col('.'))
  let match = matchstr(line[start :], '[.[:alnum:]]\+')
  let fname = findfile(match . '.pas', &path)
  return !empty(fname)
        \ ? {'path': fnamemodify(fname, ':p'), 'line': 1, 'col': 0}
        \ : 0
endfunction


" Initialization {{{1



" 1}}}
