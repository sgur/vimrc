" https://gist.github.com/kamichidu/f261b0a25021774807a9
let s:save_cpo= &cpo
set cpo&vim

let s:hook= {
\   'kind': 'hook',
\   'name': 'vspec',
\   'config': {
\       'enable': 0,
\       'vim': 'vim',
\       'runtimepath': [],
\   },
\}

function! s:hook.on_normalized(session, context)
    let runtimepath= self.config.runtimepath
    let input_script= a:session.config.srcfile

    let driver_script= [
    \ 'function s:main()',
    \ '  let standard_paths = split(&runtimepath, ",")[1 : -2]',
    \ '  let non_standard_paths = reverse([' . join(map(runtimepath, 'string(v:val)'), ',') . '])',
    \ '  let all_paths = copy(standard_paths)',
    \ '  for i in non_standard_paths',
    \ '  let all_paths = [i] + all_paths + [i . "/after"]',
    \ '  endfor',
    \ '  let &runtimepath = join(all_paths, ",")',
    \ '',
    \ '  1 verbose call vspec#test(''' . input_script . ''')',
    \ '  qall!',
    \ 'endfunction',
    \ 'call s:main()',
    \]

    let driver_path= a:session.tempname()
    call writefile(driver_script, driver_path)

    let a:session.config.command= self.config.vim
    let a:session.config.cmdopt= '-u NONE -i NONE -N -e -s -S "' . driver_path . '" 2>&1'
    let a:session.config.exec= ['%C %O']
endfunction

function! s:hook.on_output(session, context)
    let a:context.data= substitute(a:context.data, '\r', '', 'g')
endfunction

function! quickrun#hook#vspec#new()
    return deepcopy(s:hook)
endfunction

let &cpo= s:save_cpo
unlet s:save_cpo
