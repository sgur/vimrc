let s:save_cpo= &cpo
set cpo&vim

let s:hook = {
      \   'kind': 'hook'
      \ , 'name': 'repeat_last'
      \ }

function! s:hook.on_finish(session, context) abort "{{{
  if !exists('b:quickrun_config')
    let b:quickrun_config = {'type': a:session.config.type}
  endif
endfunction "}}}

function! quickrun#hook#repeat_last#new() abort
  return deepcopy(s:hook)
endfunction

let &cpo= s:save_cpo
unlet s:save_cpo
