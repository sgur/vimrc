scriptencoding utf-8



" Interface {{{1

" 実行パスのチェック
function! vimrc#executable(progname) abort
  let key = fnamemodify(a:progname, ':t')
  if !has_key(s:executable, key)
    let s:executable[key] = executable(expand(a:progname, 1))
  endif
  return s:executable[key]
endfunction

" UTF-8 記号が使えるか
let g:vimrc#has_utf8_symbol = &encoding is# 'utf-8' && (has('gui_running') || &shell =~# 'sh$')

" Internal {{{1

function! s:init_executable() abort "{{{
  let s:executable = get(g:, 'VIMRC_EXECUTABLE', {})
endfunction "}}}


" Initialization {{{1

call s:init_executable()

augroup vimrc_vimrc
  autocmd!
  autocmd VimLeavePre *  let g:VIMRC_EXECUTABLE = get(s:, 'executable', {})
augroup END

command! -nargs=0 EnvResetExecutable  call s:init_executable()


" 1}}}

