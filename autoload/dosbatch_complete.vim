scriptencoding utf-8



" Internal {{{1

" Cache creation {{{2

function! s:get_help() abort "{{{
  try
    let raw = system('cmd /c help')
    if &termencoding !=# &encoding
      let raw = iconv(raw, &termencoding, &encoding)
    endif
    return split(raw, "\n")[1:-3]
  catch /^Vim\%((\a\+)\)\=:EE677/
    return []
  endtry
endfunction "}}}

function! s:normailze(descriptions) abort "{{{
  let _ = []
  for i in range(len(a:descriptions))
    if a:descriptions[i][0] !=# ' '
      let _ += [a:descriptions[i]]
    else
      let sep = _[-1] =~ '^\a\+$' ? ' ' : ''
      let _[-1] .= sep . matchstr(a:descriptions[i], '^\s\+\zs.*')
    endif
  endfor
  return _
endfunction "}}}

function! s:export(dict) abort "{{{
  let dir = expand('~/.cache/dosbatch/')
  if !isdirectory(dir)
    call mkdir(dir, 'p')
  endif
  call writefile(map(items(a:dict), 'v:val[0] . "\t" . v:val[1]'), expand(dir . 'cache.txt'))
endfunction "}}}

function! s:init_with_cache() abort "{{{
  let fname = expand('~/.cache/dosbatch/cache.txt')
  let _ = {}
  if filereadable(fname)
    let items = readfile(fname)
    for item in items
      let [key, val] = split(item, "\t")
      let _[key] = val
    endfor
  endif
  return _
endfunction "}}}

function! s:make_cache() abort "{{{
  let _ = s:init_with_cache()
  if !empty(_)
    return _
  endif
  for str in s:normailze(s:get_help())
    let [type; desc] = split(str, '\s\+')
    let _[toupper(type)] = join(desc)
  endfor
  call s:export(_)
  return _
endfunction "}}}


" 2}}}

function! s:start_pos() abort  "{{{
  let line = getline('.')
  let col = col('.') - 1
  while col > 0 && line[col - 1] =~# '\a'
    let col -= 1
  endwhile
  return col
endfunction "}}}

function! s:case_fn(str) abort "{{{
  return a:str[0] =~# '\U' ? 'tolower' : 'toupper'
endfunction "}}}

function! s:candidates(term) abort "{{{
  return map(filter(items(s:cache), 'v:val[0] =~# "\\c^" . a:term')
        \ , '{"word": ' . s:case_fn(a:term) . '(v:val[0]), "menu": v:val[1], "icase": 1}')
endfunction "}}}


" Interface {{{1

function! dosbatch_complete#omnifunc(findstart, base) abort
  if a:findstart
    return s:start_pos()
  endif
  return s:candidates(a:base)
endfunction


" Initialization {{{1

let s:cache = s:make_cache()


" 1}}}
