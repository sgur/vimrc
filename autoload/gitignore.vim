scriptencoding utf-8



" Interface {{{1

function! gitignore#new(callback, ...) abort
  call webapi#http#defer#get(s:url . join(a:000, ','), a:callback)
endfunction

function! gitignore#complete(arglead, cmdline, cursorpos) abort
  if !exists('s:cached_filetypes')
    let s:cached_filetypes = s:get_filetypes()
  endif
  return filter(copy(s:cached_filetypes), 'stridx(v:val, a:arglead) == 0')
endfunction

function! gitignore#callback_for_new(response) abort
  if str2nr(a:response.status) != 200
    echoerr 'HTTP Error:' a:response.status
    return
  endif
  new .gitignore
  call append(0, s:extract(split(a:response.content, "\n")))
endfunction
 
function! gitignore#callback_for_read(response) abort
  if str2nr(a:response.status) != 200
    echoerr 'HTTP Error:' a:response.status
    return
  endif
  call append(line('.')-1, s:extract(split(a:response.content, "\n")))
endfunction



" Internal {{{1

function! s:trim(lines) abort "{{{
  let nr = 0
  for line in a:lines
    if line !~# '^$'
      return nr
    endif
    let nr += 1
  endfor
  return 0
endfunction "}}}

function! s:extract(lines) abort "{{{
  let lines = a:lines[s:find_emptyline(a:lines):]
  return lines[s:trim(lines):]
endfunction "}}}

function! s:find_emptyline(lines) abort "{{{
  let nr = 0
  for line in a:lines
    let nr += 1
    if line =~# '^$'
      return nr
    endif
  endfor
  return 0
endfunction "}}}

function! s:get_filetypes() abort "{{{
  let response = webapi#http#get(s:url . 'list')
  if str2nr(response.status) != 200
    echoerr 'HTTP Error:' response.status
    return [] 
  endif
  let lines = s:extract(split(response.content, "\n"))
  return split(join(lines, ','), ',')
endfunction "}}}

" Initialization {{{1

let s:url = 'https://www.gitignore.io/api/'


" 1}}}
