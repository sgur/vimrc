scriptencoding utf-8



" Interface {{{1

function! http#on_bufreadcmd(path) abort
  call s:http_get(a:path, function('s:bufread_cb'))
endfunction

function! http#on_filereadcmd(path) abort
  call s:http_get(a:path, function('s:fileread_cb'))
endfunction


" Internal {{{1

function! s:http_get(path, fn) abort
  let view = winsaveview()
  try
    call webapi#http#defer#get(a:path, a:fn)
  finally
    call winrestview(view)
  endtry
endfunction

function! s:bufread_cb(response) abort "{{{
  if str2nr(a:response.status) != 200
    echoerr a:response.status a:response.message
  endif
  set buftype=nofile
  call append(0, split(a:response.content, "\n"))
  if getline(1)[1] =~# '[[{]'
    setfiletype json
  endif
  if getline(1)[:20] =~? '<!doctype html>'
    setfiletype html
  endif
endfunction "}}}

function! s:fileread_cb(response) abort "{{{
  if str2nr(a:response.status) != 200
    echoerr a:response.status a:response.message
  endif
  call append(0, split(a:response.content, "\n"))
endfunction "}}}


" Initialization {{{1



" 1}}}
