" gitignore
" Version: 0.0.1
" Author: 
" License: 

" if exists('g:loaded_gitignore')
"   finish
" endif
" let g:loaded_gitignore = 1

let s:save_cpo = &cpo
set cpo&vim



command! -nargs=+ -complete=customlist,gitignore#complete GitignoreToCurrentDir
      \ call gitignore#new(function('gitignore#callback_for_new'), <f-args>)

command! -nargs=+ -complete=customlist,gitignore#complete GitignoreAtCursor
      \ call gitignore#new(function('gitignore#callback_for_read'), <f-args>)



let &cpo = s:save_cpo
unlet s:save_cpo

" vim:set et:
