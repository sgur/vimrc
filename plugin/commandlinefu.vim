" Description:
" commandlinefu.com から利用例を検索し previewwindow か quickfix に放流する
" Requirement:
" - webapi-vim <https://github.com/mattn/webapi-vim>
" - http://... で始まる URL を edit できるプラグイン (ex. netrw)
" Usage:
" :CmdFu [-quickfix] [-matching] {term}
"   -quickfix: quickfix に表示 (デフォルト: 未指定時は previewwindow に表示)
"   -matching: 単語にマッチする例を検索 (デフォルト: 未指定時はコマンドの使用例)scriptencoding utf-8


if exists('g:loaded_commandlinefu') && g:loaded_commandlinefu
  finish
endif
let g:loaded_commandlinefu = 1



command! -nargs=+ -complete=customlist,commandlinefu#complete CmdFu  call commandlinefu#find(<f-args>)



" 1}}}
