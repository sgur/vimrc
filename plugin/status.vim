scriptencoding utf-8

if exists('g:loaded_status') && g:loaded_status
  finish
endif
let g:loaded_status = 1


let s:save_cpo = &cpo
set cpo&vim

" Status {{{1
function! s:status(winnum) "{{{
  let active = a:winnum == winnr() || winnr('$') == 1
  " alternative buffers
  let altstat = s:get_alternative(a:winnum)
  if !empty(altstat) | return altstat | endif
  " line / column
  let curstat = strlen(string(line('$'))) . (&l:number || &l:relativenumber ? 'v' : 'l')
  let stat = [printf('%s%%%s%%*', active ? '%#CursorLineNr#' : '', curstat)]
        \ + [' %f%< <- '] + [pathshorten(getcwd())]

  if !active | return join(stat, '') | endif

  " spell/paste
  let substats = []
  if getwinvar(a:winnum, '&spell') | let substats += ['Spell'] | endif
  if getwinvar(a:winnum, '&paste') | let substats += ['Paste'] | endif

  let stat += ['::%Y%M%R%H%W', empty(substats) ? '' : ',' . join(substats, ',')]
  " git/hg branch
  let branch = s:get_branchname()
  if !empty(branch)
    let stat += [branch]
  endif
  " additional
  let stat += ['%=']
  let stat += [' %1* %{&fenc}:%{&ff}%a %*']
  let trailings = s:format_trailingspace(s:count_trailingspaces())
  if !empty(trailings)
    let stat += ['%#Error#' . trailings . '%*']
  endif
  return join(stat, '')
endfunction "}}}

" Highlight trailing spaces {{{1
function! s:format_trailingspace(nrs) "{{{
  return empty(a:nrs) ? '' : printf('[trail:%d%s]', a:nrs[0], empty(a:nrs) ? '' : '+')
endfunction "}}}

function! s:count_trailingspaces() "{{{
  return (&readonly || !&modifiable || line('$') > 5000)
        \ ? []
        \ : filter(range(1, line('$')), 'getline(v:val) =~# "\\s\\+$"')
endfunction "}}}

" Git/Hg branches {{{1
function! s:get_branchname() "{{{
  let [type, dir] = s:find_repotype(expand('%:p:h'))
  return !empty(type) ? printf(' <- %s%s', s:{type}_branch(dir), s:get_hunks()) : ''
endfunction "}}}

function! s:find_repotype(dir) "{{{
  let parent = fnamemodify(a:dir, ':h')
  if a:dir == parent
    return ['', '']
  endif
  let type = filter(map(['git', 'hg'], '[v:val, printf("%s/.%s", a:dir, v:val)]'), 'isdirectory(v:val[1])')
  if !empty(type)
    return type[0]
  endif
  return s:find_repotype(parent)
endfunction "}}}

function! s:git_branch(dir) "{{{
  return 'git:' . matchstr(readfile(a:dir . '/HEAD', 'b')[0], '\<refs/heads/\zs.\+$')
endfunction "}}}

function! s:hg_branch(dir) "{{{
  return 'hg:' . readfile(a:dir . '/branch', 'b')[0]
endfunction "}}}

function! s:get_hunk_function() abort "{{{
  return get(filter(['sign_deferred#get_stats', 'sy#repo#get_stats', 'GitGutterGetHunkSummary'], 'exists("*" . v:val)'), 0, '')
endfunction "}}}

function! s:get_hunks() " {{{
  let fn = s:get_hunk_function()
  if empty(fn)
    return ''
  endif
  let summary = call(fn, [])
  return len(summary) == 3 ? printf('[+%s -%s ~%s]', summary[0], summary[2], summary[1]) : ''
endfunction " }}}

" Alternative buffers {{{1
function! s:get_alternative(winnum) "{{{
  let bufnum = winbufnr(a:winnum)
  let buftype = getbufvar(bufnum, '&buftype')
  let bufname = bufname(bufnum)
  let filetype = getbufvar(bufnum, '&filetype')

  if buftype is# 'help'
    let lang = fnamemodify(bufname, ':e')
    return ' HELP ' . fnamemodify(bufname, ':t:r') . (lang[2] is# 'x' ? '@' . lang[:1] : '')
  elseif filetype is# 'netrw'
    return ' NETRW ' . fnamemodify(bufname, ':p')
  elseif buftype is# 'quickfix'
    return ' %q (' . max([len(getqflist()), len(getloclist(a:winnum))]) . ')'
  elseif bufname is# '__Gundo__'
    return ' Gundo'
  elseif bufname is# '__Gundo_Preview__'
    return ' Gundo Preview'
  endif
  return ''
endfunction "}}}

" Update status {{{1
function! s:refresh_status() abort "{{{
  if has('vim_starting') | return | endif
  let sid = matchstr(expand('<sfile>'), '<SNR>\d\+_')
  for nr in range(1, winnr('$'))
    if !empty(getbufvar(winbufnr(nr), '&buftype'))
      continue
    endif
    call setwinvar(nr, '&statusline', '%!' . sid . 'status(' . nr . ')')
  endfor
endfunction "}}}

augroup vimrc_status "{{{
  autocmd!
  autocmd BufReadPost,BufWritePost,VimEnter,VimLeave,WinEnter,WinLeave,BufWinEnter,BufWinLeave *  call s:refresh_status()
augroup END "}}}
" 1}}}

let &cpo = s:save_cpo
unlet s:save_cpo
