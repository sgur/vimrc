" fontsize
" Version: 0.0.1
" Author: 
" License: 

if exists('g:loaded_fontsize')
  finish
endif
let g:loaded_fontsize = 1

let s:save_cpo = &cpo
set cpo&vim


" Commands.
command! -narg=? -bang -bar Fontsize call fontsize#set(<q-args>, <bang>0)

" Key mappings.
nnoremap <silent> <Plug>(Fontsize-larger)
      \ :<C-u>Fontsize +<C-r>=v:count1<CR><CR>
nnoremap <silent> <Plug>(Fontsize-smaller)
      \ :<C-u>Fontsize -<C-r>=v:count1<CR><CR>


if !get(g:, 'fontsize_no_default_key_mappings', 0)
  silent! nmap <unique> <silent> + <Plug>(Fontsize-larger)
  silent! nmap <unique> <silent> - <Plug>(Fontsize-smaller)
endif


let &cpo = s:save_cpo
unlet s:save_cpo

" vim:set et:
