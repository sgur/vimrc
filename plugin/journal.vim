scriptencoding utf-8

" if exists('g:loaded_journal') && g:loaded_journal
"   finish
" endif
let g:loaded_journal = 1

function! s:add_entry(...) abort "{{{
  call append(0, ['#  @ ' . strftime('%c'), '', '', ''])
  call cursor(1, 3)
  if a:0
    call append(line('.')+2, ['```'] + a:1 + ['```'])
  endif
  setlocal nomodified
endfunction "}}}

function! s:fname() abort "{{{
  return expand(get(g:, 'journal_path', '~/journal.md'))
endfunction "}}}

function! s:edit() abort "{{{
  let journal_path = s:fname()
  let winnrs = filter(range(1, winnr('$')), 'expand("#" . winbufnr(v:val) . ":p") == journal_path')
  if !empty(winnrs)
    execute winnrs[0] 'wincmd w'
  else
    execute get(g:, 'journal_edit_cmd', 'edit') journal_path
  endif
  command! -buffer AddEntry  call s:add_entry()
  if exists(':JournalAddEntry')
    command! -buffer JournalAddEntry  call s:add_entry()
  endif
endfunction "}}}

function! s:cmd_add_entry(...) range abort "{{{
  let lines = a:0 > 1 && a:1 != a:2 ? getline(a:1, a:2) : []
  call s:edit()
  call s:add_entry(lines)
endfunction "}}}

function! s:cmd_edit() abort "{{{
  call s:edit()
endfunction "}}}

command! -nargs=0 -range JournalAddEntry  call s:cmd_add_entry(<line1>, <line2>)
command! -nargs=0 JournalEdit  call s:cmd_edit()
