setlocal tabstop=4 shiftwidth=4 expandtab


let b:undo_ftplugin = (exists('b:undo_ftplugin')? b:undo_ftplugin . '|': '')
      \ . "setlocal ts< sw< et<"
